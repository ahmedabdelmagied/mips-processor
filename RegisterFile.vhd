----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:40:38 04/02/2017 
-- Design Name: 
-- Module Name:    RegisterFile - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.pack.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RegisterFile is port(
read_sel1 : in std_logic_vector(4 downto 0);
read_sel2 : in std_logic_vector(4 downto 0);
write_sel : in std_logic_vector(4 downto 0);
write_ena : in std_logic;
clk       : in std_logic;
write_data: in std_logic_vector(31 downto 0);
data1: out std_logic_vector(31 downto 0);
data2: out std_logic_vector(31 downto 0));
end RegisterFile;

architecture Behavioral of RegisterFile is
signal out1  : std_logic_vector(31 downto 0);
signal out2  : std_logic_vector(31 downto 0);
signal out3  : std_logic_vector(31 downto 0);
signal out4  : std_logic_vector(31 downto 0);
signal out5  : std_logic_vector(31 downto 0);
signal out6  : std_logic_vector(31 downto 0);
signal out7  : std_logic_vector(31 downto 0);
signal out8  : std_logic_vector(31 downto 0);
signal out9  : std_logic_vector(31 downto 0);
signal out10 : std_logic_vector(31 downto 0);
signal out11 : std_logic_vector(31 downto 0);
signal out12 : std_logic_vector(31 downto 0);
signal out13 : std_logic_vector(31 downto 0);
signal out14 : std_logic_vector(31 downto 0);
signal out15 : std_logic_vector(31 downto 0);
signal out16 : std_logic_vector(31 downto 0);
signal out17 : std_logic_vector(31 downto 0);
signal out18 : std_logic_vector(31 downto 0);
signal out19 : std_logic_vector(31 downto 0);
signal out20 : std_logic_vector(31 downto 0);
signal out21 : std_logic_vector(31 downto 0);
signal out22 : std_logic_vector(31 downto 0);
signal out23 : std_logic_vector(31 downto 0);
signal out24 : std_logic_vector(31 downto 0);
signal out25 : std_logic_vector(31 downto 0);
signal out26 : std_logic_vector(31 downto 0);
signal out27 : std_logic_vector(31 downto 0);
signal out28 : std_logic_vector(31 downto 0);
signal out29 : std_logic_vector(31 downto 0);
signal out30 : std_logic_vector(31 downto 0);
signal out31 : std_logic_vector(31 downto 0);
signal out32 : std_logic_vector(31 downto 0);
------------------------------------------------------

signal decoderout : std_logic_vector(31 downto 0);

begin

	decoder : decode port map (write_sel , decoderout ,write_ena);
	mux1 : mux port map (out1 , out2 ,out3,out4,out5,out6,out7,out8,out9,out10,out11,out12,out13,out14,out15,out16,out17,out18,out19,out20,out21 ,out22,out23,out24,out25,out26,out27,out28,out29,out30,out31,out32,  read_sel1 , data1 );
	mux2 : mux port map (out1 , out2 ,out3,out4,out5,out6,out7,out8,out9,out10,out11,out12,out13,out14,out15,out16,out17,out18,out19,out20,out21 ,out22,out23,out24,out25,out26,out27,out28,out29,out30,out31,out32,  read_sel2 , data2 );

	R0 : reg32_0 port map(write_data,clk,decoderout(0),'0','0',out1);
	R1 : reg32_1 port map(write_data,clk,decoderout(1),'0','0',out2);
	R2 : reg generic map(32) port map(write_data,clk,decoderout(2),'0','0',out3);
	R3 : reg generic map(32) port map(write_data,clk,decoderout(3),'0','0',out4);
	R4 : reg generic map(32) port map(write_data,clk,decoderout(4),'0','0',out5);
	R5 : reg generic map(32) port map(write_data,clk,decoderout(5),'0','0',out6);
	R6 : reg generic map(32) port map(write_data,clk,decoderout(6),'0','0',out7);
	R7 : reg generic map(32) port map(write_data,clk,decoderout(7),'0','0',out8);
	R8 : reg generic map(32) port map(write_data,clk,decoderout(8),'0','0',out9);
	R9 : reg generic map(32) port map(write_data,clk,decoderout(9),'0','0',out10);
	R10: reg generic map(32) port map(write_data,clk,decoderout(10),'0','0',out11);
	R11: reg generic map(32) port map(write_data,clk,decoderout(11),'0','0',out12);
	R12: reg generic map(32) port map(write_data,clk,decoderout(12),'0','0',out13);
	R13: reg generic map(32) port map(write_data,clk,decoderout(13),'0','0',out14);
	R14: reg generic map(32) port map(write_data,clk,decoderout(14),'0','0',out15);
	R15: reg generic map(32) port map(write_data,clk,decoderout(15),'0','0',out16);
	R16: reg generic map(32) port map(write_data,clk,decoderout(16),'0','0',out17);
	R17: reg generic map(32) port map(write_data,clk,decoderout(17),'0','0',out18);
	R18: reg generic map(32) port map(write_data,clk,decoderout(18),'0','0',out19);
	R19: reg generic map(32) port map(write_data,clk,decoderout(19),'0','0',out20);
	R20: reg generic map(32) port map(write_data,clk,decoderout(20),'0','0',out21);
	R21: reg generic map(32) port map(write_data,clk,decoderout(21),'0','0',out22);
	R22: reg generic map(32) port map(write_data,clk,decoderout(22),'0','0',out23);
	R23: reg generic map(32) port map(write_data,clk,decoderout(23),'0','0',out24);
	R24: reg generic map(32) port map(write_data,clk,decoderout(24),'0','0',out25);
	R25: reg generic map(32) port map(write_data,clk,decoderout(25),'0','0',out26);
	R26: reg generic map(32) port map(write_data,clk,decoderout(26),'0','0',out27);
	R27: reg generic map(32) port map(write_data,clk,decoderout(27),'0','0',out28);
	R28: reg generic map(32) port map(write_data,clk,decoderout(28),'0','0',out29);
	R29: reg generic map(32) port map(write_data,clk,decoderout(29),'0','0',out30);
	R30: reg generic map(32) port map(write_data,clk,decoderout(30),'0','0',out31);
	R31: reg generic map(32) port map(write_data,clk,decoderout(31),'0','0',out32);



end Behavioral;

