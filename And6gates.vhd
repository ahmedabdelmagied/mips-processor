----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:03:36 04/30/2017 
-- Design Name: 
-- Module Name:    And6gates - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity And6gates is
    Port ( i : in  STD_LOGIC_VECTOR (5 downto 0);
           o : out  STD_LOGIC);
end And6gates;

architecture Behavioral of And6gates is

begin
o <= '1' when i = "111111"else
    '0' ;

end Behavioral;

