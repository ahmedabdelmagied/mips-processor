----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:10:03 03/28/2017 
-- Design Name: 
-- Module Name:    ALU - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.Pack.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ALU is
    Port ( data1 : in  STD_LOGIC_VECTOR (31 downto 0);
           data2 : in  STD_LOGIC_VECTOR (31 downto 0);
           aluop : in  STD_LOGIC_VECTOR (3 downto 0);
           cin : in  STD_LOGIC;
           dataout : out  STD_LOGIC_VECTOR (31 downto 0);
           cflag : out  STD_LOGIC;
           zflag : out  STD_LOGIC;
           oflag : out  STD_LOGIC);
end ALU;

architecture Behavioral of ALU is

SIGNAL x : STD_LOGIC :='0';
SIGNAL y : STD_LOGIC :='0';

SIGNAL Zvec: STD_LOGIC_VECTOR (31 DOWNTO 0);
SIGNAL In1: STD_LOGIC_VECTOR (31 DOWNTO 0);
SIGNAL In2: STD_LOGIC_VECTOR (31 DOWNTO 0);
SIGNAL Bits: STD_LOGIC_VECTOR (31 DOWNTO 0);
SIGNAL Carry: STD_LOGIC_VECTOR (31 DOWNTO 0);
SIGNAL set: STD_LOGIC_VECTOR (31 DOWNTO 0);

Signal x0 : Std_logic;
Signal x1 : Std_logic;
Signal x2 : Std_logic;
Signal x3 : Std_logic;
Signal x4 : Std_logic;
Signal x5 : Std_logic;
Signal x6 : Std_logic;
Signal x7 : Std_logic;
Signal x8 : Std_logic;
Signal x9 : Std_logic;
Signal x10 : Std_logic;
Signal x11 : Std_logic;
Signal x12 : Std_logic;
Signal x13 : Std_logic;
Signal x14 : Std_logic;
Signal x15 : Std_logic;
Signal x16 : Std_logic;
Signal x17 : Std_logic;
Signal x18 : Std_logic;
Signal x19 : Std_logic;
Signal x20 : Std_logic;
Signal x21 : Std_logic;
Signal x22 : Std_logic;
Signal x23 : Std_logic;
Signal x24 : Std_logic;
Signal x25 : Std_logic;
Signal x26 : Std_logic;
Signal x27 : Std_logic;
Signal x28 : Std_logic;
Signal x29 : Std_logic;
Signal x30 : Std_logic;
Signal x31 : Std_logic;

Signal ZZ : Std_logic :='0';
Signal xs : Std_logic;
Signal cs : Std_logic;

begin

y<=aluop(3);
M1 : Mux_2x1 PORT MAP(data1,NOT(data1),y,In1);

x<=aluop(2);
M2 : Mux_2x1 PORT MAP(data2,NOT(data2),x,In2);

A0 : SmallALU  PORT MAP(In1(0),In2(0),ZZ,aluop,cin,xs,cs,set(0));
A1 : SmallALU  PORT MAP(In1(1),In2(1),ZZ,aluop,Carry(0),x1,Carry(1),set(1));
A2 : SmallALU  PORT MAP(In1(2),In2(2),ZZ,aluop,Carry(1),x2,Carry(2),set(2));
A3 : SmallALU  PORT MAP(In1(3),In2(3),ZZ,aluop,Carry(2),x3,Carry(3),set(3));
A4 : SmallALU  PORT MAP(In1(4),In2(4),ZZ,aluop,Carry(3),x4,Carry(4),set(4));
A5 : SmallALU  PORT MAP(In1(5),In2(5),ZZ,aluop,Carry(4),x5,Carry(5),set(5));
A6 : SmallALU  PORT MAP(In1(6),In2(6),ZZ,aluop,Carry(5),x6,Carry(6),set(6));
A7 : SmallALU  PORT MAP(In1(7),In2(7),ZZ,aluop,Carry(6),x7,Carry(7),set(7));
A8 : SmallALU  PORT MAP(In1(8),In2(8),ZZ,aluop,Carry(7),x8,Carry(8),set(8));
A9 : SmallALU  PORT MAP(In1(9),In2(9),ZZ,aluop,Carry(8),x9,Carry(9),set(9));
A10 : SmallALU  PORT MAP(In1(10),In2(10),ZZ,aluop,Carry(9),x10,Carry(10),set(10));
A11 : SmallALU  PORT MAP(In1(11),In2(11),ZZ,aluop,Carry(10),x11,Carry(11),set(11));
A12 : SmallALU  PORT MAP(In1(12),In2(12),ZZ,aluop,Carry(11),x12,Carry(12),set(12));
A13 : SmallALU  PORT MAP(In1(13),In2(13),ZZ,aluop,Carry(12),x13,Carry(13),set(13));
A14 : SmallALU  PORT MAP(In1(14),In2(14),ZZ,aluop,Carry(13),x14,Carry(14),set(14));
A15 : SmallALU  PORT MAP(In1(15),In2(15),ZZ,aluop,Carry(14),x15,Carry(15),set(15));
A16 : SmallALU  PORT MAP(In1(16),In2(16),ZZ,aluop,Carry(15),x16,Carry(16),set(16));
A17 : SmallALU  PORT MAP(In1(17),In2(17),ZZ,aluop,Carry(16),x17,Carry(17),set(17));
A18 : SmallALU  PORT MAP(In1(18),In2(18),ZZ,aluop,Carry(17),x18,Carry(18),set(18));
A19 : SmallALU  PORT MAP(In1(19),In2(19),ZZ,aluop,Carry(18),x19,Carry(19),set(19));
A20 : SmallALU  PORT MAP(In1(20),In2(20),ZZ,aluop,Carry(19),x20,Carry(20),set(20));
A21 : SmallALU  PORT MAP(In1(21),In2(21),ZZ,aluop,Carry(20),x21,Carry(21),set(21));
A22 : SmallALU  PORT MAP(In1(22),In2(22),ZZ,aluop,Carry(21),x22,Carry(22),set(22));
A23 : SmallALU  PORT MAP(In1(23),In2(23),ZZ,aluop,Carry(22),x23,Carry(23),set(23));
A24 : SmallALU  PORT MAP(In1(24),In2(24),ZZ,aluop,Carry(23),x24,Carry(24),set(24));
A25 : SmallALU  PORT MAP(In1(25),In2(25),ZZ,aluop,Carry(24),x25,Carry(25),set(25));
A26 : SmallALU  PORT MAP(In1(26),In2(26),ZZ,aluop,Carry(25),x26,Carry(26),set(26));
A27 : SmallALU  PORT MAP(In1(27),In2(27),ZZ,aluop,Carry(26),x27,Carry(27),set(27));
A28 : SmallALU  PORT MAP(In1(28),In2(28),ZZ,aluop,Carry(27),x28,Carry(28),set(28));
A29 : SmallALU  PORT MAP(In1(29),In2(29),ZZ,aluop,Carry(28),x29,Carry(29),set(29));
A30 : SmallALU  PORT MAP(In1(30),In2(30),ZZ,aluop,Carry(29),x30,Carry(30),set(30));
A31 : SmallALU  PORT MAP(In1(31),In2(31),ZZ,aluop,Carry(30),x31,Carry(31),set(31));

Af : SmallALU  PORT MAP(In1(0),In2(0),set(31),aluop,cin,x0,Carry(0));

Bits<=(x31 & x30 & x29 & x28 & x27 & x26 & x25 & 
       x24 &  x23 &  x22 &  x21 &  x20 &  x19 & x18 &
		 x17 & x16 & x15 & x14 & x13 & x12 & x11 &
		 x10 & x9 & x8 & x7 & x6 & x5 & x4 &
		 x3 & x2 & x1 & x0 );

dataout<=Bits;
Zvec<=Bits;

cflag<=Carry(31);
		 
oflag<=(Carry(31) Xor Carry(30));

zflag<=Not(Zvec(31) OR Zvec(30) OR Zvec(29) OR Zvec(28) OR Zvec(27) OR Zvec(26) OR Zvec(25) OR 
       Zvec(24) OR  Zvec(23) OR  Zvec(22) OR  Zvec(21) OR  Zvec(20) OR  Zvec(19) OR Zvec(18) OR
		 Zvec(17) OR Zvec(16) OR Zvec(15) OR Zvec(14) OR Zvec(13) OR Zvec(12) OR Zvec(11) OR
		 Zvec(10) OR Zvec(9) OR Zvec(8) OR Zvec(7) OR Zvec(6) OR Zvec(5) OR Zvec(4) OR
		 Zvec(3) OR Zvec(2) OR Zvec(1) OR Zvec(0) );

end Behavioral;
