----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:12:12 04/30/2017 
-- Design Name: 
-- Module Name:    ALUControl - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ALUControl is
    Port ( ALUOP : in  STD_LOGIC_VECTOR (1 downto 0);
           F : in  STD_LOGIC_VECTOR (5 downto 0);
           Operation : out  STD_LOGIC_VECTOR (3 downto 0));
end ALUControl;

architecture Behavioral of ALUControl is

Signal s1 : STD_LOGIC ;
Signal s2 : STD_LOGIC ;
Signal s3 : STD_LOGIC ;
Signal s4 : STD_LOGIC ;
Signal s5 : STD_LOGIC ;

begin

s1 <= F(3)or F(0) ;

s5 <= s1 and ALUOP(1) ;

Operation(0) <= not s4 and s5;

Operation(1) <= not F(2) or not ALUOP(1) ;

s2 <= F(1) and ALUOP(1) ;

Operation(2) <= s2 Or ALUOP(0) ; 


s3 <= F(1) and F(0) ;

s4 <= S3 and ALUOP(1) ;

Operation(3) <= s4 ;

end Behavioral;

