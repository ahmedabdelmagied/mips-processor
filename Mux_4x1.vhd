----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:51:50 03/28/2017 
-- Design Name: 
-- Module Name:    Mux_4x1 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Mux_4x1 is
    Port ( choice0 : in  STD_LOGIC;
           choice1 : in  STD_LOGIC;
           choice2 : in  STD_LOGIC;
           choice3 : in  STD_LOGIC;
           sel : in  STD_LOGIC_VECTOR (1 downto 0);
           output : out  STD_LOGIC);
end Mux_4x1;

architecture Behavioral of Mux_4x1 is

begin
    
output<=choice0 when(sel="00") else
         choice1 when(sel="01") else
         choice2 when(sel="10") else
         choice3 when(sel="11");
	 
	

end Behavioral;

