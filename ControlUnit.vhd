----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:58:20 04/30/2017 
-- Design Name: 
-- Module Name:    ControlUnit - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.Pack.all ;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ControlUnit is
    Port ( Op : in  STD_LOGIC_VECTOR (5 downto 0);
           RegDst : out  STD_LOGIC;
			  jump : out STD_LOGIC ;
           AluSrc  : out STD_LOGIC ;
			  Memtoreg : out  STD_LOGIC;
           Regwrite : out  STD_LOGIC;
           Memread : out  STD_LOGIC;
           Memwrite : out  STD_LOGIC;
           Branch : out  STD_LOGIC;
			  Branchnot : out STD_LOGIC;
           Aluop : out  STD_LOGIC_VECTOR (1 downto 0));
end ControlUnit;

architecture Behavioral of ControlUnit is

signal rform : std_logic ;
signal lwo : std_logic ;
signal swo : std_logic ;
signal beqo : std_logic ;
signal bneo : std_logic ;

begin

Rformat : And6gates port map (not Op(5)&not Op(4)& not Op(3)& not Op(2) &not Op(1) &not Op(0) , rform );

RegDst <= rform ;
Aluop(1) <= rform ;

lw 	  : And6gates port map ( Op(5) & not Op(4) &not Op(3) & not Op(2) & Op(1) & Op(0) ,lwo  );
Memtoreg <= lwo ;
Memread  <= lwo ;
Regwrite <= lwo or rform ;

sw 	  : And6gates port map ( Op(5) & not Op(4) & Op(3) & not Op(2) & Op(1) & Op(0) ,swo  );
AluSrc <= swo Or lwo ;
Memwrite <= swo ;

beq 	  : And6gates port map ( not Op(5) & not Op(4) &not Op(3) &  Op(2) & not Op(1) &not Op(0) ,beqo  );
Branch <= beqo or bneo ;
Aluop(0) <= beqo or bneo ;

bne 	  : And6gates port map ( not Op(5) & not Op(4) &not Op(3) &  Op(2) & not Op(1) & Op(0) ,bneo  );

Branchnot <= bneo ;
j 	  : And6gates port map ( not Op(5) & not Op(4) &not Op(3) & not Op(2) &  Op(1) &not Op(0) , jump  );


end Behavioral;

