----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:25:21 04/19/2017 
-- Design Name: 
-- Module Name:    MainModule - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.Pack.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MainModule is
port (
	      START : IN  std_logic;
         CLK : IN  std_logic;
         RegFileOut1 : OUT  std_logic_vector(31 downto 0);
         RegFileOut2 : OUT  std_logic_vector(31 downto 0);
			ALUOut : OUT  std_logic_vector(31 downto 0);
			PCOut: OUT STD_LOGIC_VECTOR(31 downto 0);
			DataMemOut: OUT STD_LOGIC_VECTOR(31 downto 0) );
end MainModule;

architecture Behavioral of MainModule is

SIGNAL pc_in  : STD_LOGIC_VECTOR ( 31 DOWNTO 0) := (others => '0')  ;
SIGNAL data : STD_LOGIC_VECTOR ( 31 DOWNTO 0) ;
SIGNAL reg1 : STD_LOGIC_VECTOR ( 31 DOWNTO 0) ;
SIGNAL reg2 : STD_LOGIC_VECTOR ( 31 DOWNTO 0) ;
SIGNAL aluoperate: STD_LOGIC_VECTOR ( 3 DOWNTO 0) ;
SIGNAL func : STD_LOGIC_VECTOR ( 5 DOWNTO 0) ;

SIGNAL output : STD_LOGIC_VECTOR ( 31 DOWNTO 0);

SIGNAL cflag : STD_LOGIC;
SIGNAL zflag : STD_LOGIC;
SIGNAL oflag : STD_LOGIC;

signal pc_out :STD_LOGIC_VECTOR(31 downto 0);
signal memoryaddress :STD_LOGIC_VECTOR(31 downto 0);

signal rs :STD_LOGIC_VECTOR(4 downto 0);
signal rt :STD_LOGIC_VECTOR(4 downto 0);
signal rd :STD_LOGIC_VECTOR(4 downto 0);

signal RegDst 	 :STD_LOGIC;
signal jump   	 :STD_LOGIC;
signal AluSrc 	 :STD_LOGIC;
signal Memtoreg :STD_LOGIC;
signal Regwrite :STD_LOGIC;
signal Memread  :STD_LOGIC;
signal Memwrite :STD_LOGIC;
signal Branch   :STD_LOGIC;
signal Branchnot:STD_LOGIC;
signal Aluop 	 :STD_LOGIC_VECTOR (1 downto 0);

signal DestReg :  STD_LOGIC_VECTOR (31 downto 0 );
signal parm2 :  STD_LOGIC_VECTOR (31 downto 0 );
signal address : STD_LOGIC_VECTOR (31 downto 0 );
signal memout : STD_LOGIC_VECTOR (31 downto 0 );
signal writedata : STD_LOGIC_VECTOR (31 downto 0 );

signal NextIns : STD_LOGIC_VECTOR (31 downto 0 );

signal SelFlag : STD_LOGIC_VECTOR (31 downto 0 );

signal BranchSel : STD_LOGIC ;
signal BranchAddress: STD_LOGIC_VECTOR (31 downto 0 );
signal Branchout : STD_LOGIC_VECTOR (31 downto 0 );

Signal Dispalcement : STD_LOGIC_VECTOR (31 downto 0 );
signal JumpAddress  : STD_LOGIC_VECTOR (31 downto 0 );
signal NewAddress   : STD_LOGIC_VECTOR (31 downto 0 );

begin

PC: reg GENERIC MAP (32) port map (pc_in ,CLK, START, '0', not START  ,pc_out);

PCOut <= pc_Out ;

ad4 : Add4  port map (pc_out , "00000000000000000000000000000100" , NextIns  );

rs <= data(25 downto 21 );
rt <= data(20 downto 16 );
rd <= data(15 downto 11 );



IM : INSTRMEMORY Generic map (64,32,32) port map (not START ,data,pc_Out,CLK);

CU : ControlUnit port map ( data(31 downto 26) , RegDst ,jump ,AluSrc , Memtoreg ,Regwrite , Memread , Memwrite ,Branch ,Branchnot ,Aluop);

DestSelector : Mux_2x1 port map ("000000000000000000000000000"&rt , "000000000000000000000000000"&rd , RegDst ,  DestReg  );
rf : registerfile port map ( rs , rt, DestReg(4 downto 0) ,Regwrite, CLK , writedata  ,reg1 , reg2 );

RegFileOut1 <= reg1 ;
RegFileOut2 <= reg2 ;

SE : Signextend port map ( data(15 downto 0), address );

AluScrSel : Mux_2x1 port map (reg2 , address , AluSrc, parm2);

func <= data (5 downto 0) ;

AluCtrl : ALUControl port map ( Aluop ,func  , aluoperate );

alu0 : ALU port map ( reg1 ,parm2 , aluoperate , aluoperate(2) ,output ,cflag,zflag ,  oflag );

ALUOut <=output;

DM : DATAMEMORY Generic Map ( 64 ,32,32) port map (not START , reg2 , memout , Memread , Memwrite ,output ,clk );

DataMemOut <= memout ;

MemtoRegSel : Mux_2x1 port map (output , memout , Memtoreg, writedata);

BranchNotSel : Mux_2x1 port map ("0000000000000000000000000000000"& zflag  , "0000000000000000000000000000000"& not zflag , Branchnot, SelFlag);

BranchSel <= Branch and SelFlag(0) ;

sl2branch : Shift2 port map (address , BranchAddress  );


BranchAdd : Add4 port map (NextIns ,BranchAddress , Dispalcement);

BranchMux : Mux_2x1 port map (NextIns , Dispalcement , BranchSel, Branchout);


sl2jump : Shift2 port map ( "000000"&data(25 downto 0) , JumpAddress  );

NewAddress <= NextIns(31 downto 28) &JumpAddress (27 downto 0);

JumpMux : Mux_2x1 port map (Branchout , NewAddress , jump, pc_in);


end Behavioral;

