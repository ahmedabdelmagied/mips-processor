----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    23:18:40 03/28/2017 
-- Design Name: 
-- Module Name:    SmallALU - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.Pack.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SmallALU is
    Port (data1 : in  STD_LOGIC;
           data2 : in  STD_LOGIC;
			  Less : in  STD_LOGIC;
           aluop : in  STD_LOGIC_VECTOR (3 downto 0);
           cin : in  STD_LOGIC;
			  dataout : out  STD_LOGIC;
			  carryout : out STD_LOGIC;
			  set : out STD_LOGIC);
end SmallALU;

architecture Behavioral of SmallALU is

Signal cout : STD_LOGIC := '0';
Signal Tmp : STD_LOGIC := '0';
signal code: STD_LOGIC_Vector (1 DOWNTO 0);
begin
 
code<= aluop(1 downto 0);

A0 : FullAdder PORT MAP(data1,data2,cin,Tmp,cout);

M4 : Mux_4x1 PORT MAP((data1 and data2),(data1 or data2),Tmp,Less,code,dataout);

set<=Tmp;

carryout <='0' when(aluop="0000") else
           '0' when(aluop="0001") else
			  cout;


end Behavioral;

