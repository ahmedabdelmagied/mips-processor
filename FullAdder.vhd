----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:16:53 03/28/2017 
-- Design Name: 
-- Module Name:    FullAdder - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FullAdder is
    Port ( A : in  STD_LOGIC;
           B : in  STD_LOGIC;
			  C: in  STD_LOGIC;
           S : out  STD_LOGIC;
           Carry : out  STD_LOGIC);
end FullAdder;

architecture Behavioral of FullAdder is
  SIGNAL Tmp: STD_LOGIC_VECTOR (1 DOWNTO 0);
  SIGNAL CA: STD_LOGIC_VECTOR (1 DOWNTO 0):="00";
  SIGNAL CB: STD_LOGIC_VECTOR (1 DOWNTO 0):="00";
  SIGNAL CC: STD_LOGIC_VECTOR (1 DOWNTO 0):="00";  
begin

CA<=('0'& A);
CB<=('0'&B);
CC<=('0'&C);
Tmp <= CA + CB + CC;
S <= Tmp(0) ;
Carry <= Tmp(1);

end Behavioral;

